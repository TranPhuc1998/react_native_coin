import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const GET_AXIOS = async (endpoint: string, body = {}) => {
  const instance = axios.create({
    baseURL: endpoint,
    timeout: 3000,
  });
  const token = await AsyncStorage.getItem('jwt');
  if (token) {
    instance.defaults.headers.Authorization = 'Bearer ' + token;
  }
  instance.defaults.headers.lang = 'vn';
  instance.defaults.headers['Content-Type'] = 'application/json';
  return instance.get('', body);

};
