import React from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';
import {hScale} from '../../Themes/Responsive';
type IHeader = {
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  textMid: string;
};
const Header: React.FC<IHeader> = ({textMid, titleStyle}) => {
  return (
    <View>
      <Text style={[styles.header, titleStyle]}>{textMid}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    color: '#f04155',

    textAlign: 'center',
    paddingTop: hScale(25),
    // fontWeight: 'bold',
  },
});
