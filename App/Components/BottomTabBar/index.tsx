import {
  BottomTabBarOptions,
  BottomTabBarProps,
} from '@react-navigation/bottom-tabs';
import React, {Fragment, useRef} from 'react';
import {
  View,
  TouchableOpacity,
  Dimensions,
  Animated,
  SafeAreaView,
  StyleSheet,
  Platform,
} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import * as shape from 'd3-shape';
import {TabActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';
import {DefaultTheme} from '../../Themes';
import {hScale} from '../../Themes/Responsive';

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const icons = ['local-grocery-store', 'local-grocery-store'];
const {width} = Dimensions.get('screen');
const tabHeight = hScale(55);

const BottomTabBar: React.FC<
  BottomTabBarProps<BottomTabBarOptions>
> = props => {
  const {state, navigation} = props;
  const {routes} = state;

  const tabWidth = hScale(80);
  const values = useRef(
    routes.map((_route, index) => new Animated.Value(index === 0 ? 1 : 0)),
  );
  const value = useRef(new Animated.Value(0));
  const translateX = value.current.interpolate({
    inputRange: [0, width],
    outputRange: [-width, 0],
  });

  const getPath = (): string => {
    const left = shape
      .line()
      .x(d => d[0])
      .y(d => d[1])([
      [0, 0],
      [width, 0],
    ]);
    const tab = shape
      .line()
      .x(d => d[0])
      .y(d => d[1])
      .curve(shape.curveBasis)([
      [width, 0],
      [width + (width / 4 - tabWidth - 10) / 2, 0],
      [width + (width / 4 - tabWidth) / 2, tabHeight / 1.5],
      [width + (width / 4 - tabWidth) / 2 + tabWidth / 2, tabHeight * 1.05],
      [width + (width / 4 - tabWidth) / 2 + tabWidth, tabHeight / 1.5],
      [width + (width / 4 - tabWidth + 10) / 2 + tabWidth, 0],
      [width + width / 4, 0],
    ]);
    const right = shape
      .line()
      .x(d => d[0])
      .y(d => d[1])([
      [width + width / 4, 0],
      [width * 2 + width / 4, 0],
      [width * 2 + width / 4, tabHeight],
      [0, tabHeight],
      [0, 0],
    ]);
    return `${left} ${tab} ${right}`;
  };

  const d = getPath();

  return (
    <SafeAreaView style={[styles.container]}>
      <AnimatedSvg
        width={width * 2 + width / 4}
        style={[
          styles.svg,
          {
            transform: [{translateX}],
          },
        ]}>
        <Path
          stroke={Platform.OS === 'android' ? 'red' : undefined}
          fill={DefaultTheme.colors.card}
          {...{d}}
        />
      </AnimatedSvg>
      <View style={StyleSheet.absoluteFill}>
        <View style={styles.tabs}>
          {routes.map((route, index) => {
            const translateY = values.current[index].interpolate({
              inputRange: [0, 1],
              outputRange: [100, 0],
              extrapolate: 'clamp',
            });
            const opacity1 = values.current[index].interpolate({
              inputRange: [0, 1],
              outputRange: [0, 1],
              extrapolate: 'clamp',
            });

            const onPress = (i: number) => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!event.defaultPrevented) {
                navigation.dispatch({
                  ...TabActions.jumpTo(route.name),
                  target: state.key,
                });
              }
              Animated.sequence([
                Animated.parallel(
                  values.current.map(v =>
                    Animated.timing(v, {
                      toValue: 0,
                      duration: 100,
                      useNativeDriver: true,
                    }),
                  ),
                ),
                Animated.parallel([
                  Animated.spring(value.current, {
                    toValue: (width / 4) * i,
                    useNativeDriver: true,
                  }),
                  Animated.spring(values.current[i], {
                    toValue: 1,
                    useNativeDriver: true,
                  }),
                ]),
              ]).start();
            };

            return (
              <Fragment key={route.key}>
                <TouchableOpacity onPress={() => onPress(index)}>
                  <Animated.View style={[styles.tab]}>
                    <Icon name={icons[index]} size={30} color={'black'} />
                  </Animated.View>
                </TouchableOpacity>
                <Animated.View
                  style={[
                    styles.activeView,
                    {
                      left: (width / 4) * index + (width / 4 - tabWidth) / 2,
                      width: tabWidth,
                      height: tabHeight,
                      opacity: opacity1,
                      transform: [{translateY}],
                    },
                  ]}>
                  <View style={styles.activeIcon}>
                    <Icon name={icons[index]} size={30} color={'white'} />
                  </View>
                </Animated.View>
              </Fragment>
            );
          })}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default BottomTabBar;
