import {hScale, wScale} from '../../Themes/Responsive';
import {StyleSheet, Dimensions, Platform} from 'react-native';
import DefaultTheme from '../../Themes/DefaultTheme';

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    bottom: Platform.OS === 'android' ? 1 : 0,
  },

  svg: {
    height: hScale(55),
    shadowColor: DefaultTheme.colors.primary,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 1,
  },

  tabs: {
    flexDirection: 'row',
  },

  tab: {
    justifyContent: 'center',
    alignItems: 'center',
    height: hScale(55),
    width: width / 4,
  },

  icon: {
    width: wScale(30),
    height: hScale(30),
  },

  activeIcon: {
    backgroundColor: DefaultTheme.colors.primary,
    width: hScale(80),
    height: hScale(80),
    borderRadius: 999,
    justifyContent: 'center',
    alignItems: 'center',
  },

  activeView: {
    position: 'absolute',
    bottom: hScale(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
