import 'react-native-gesture-handler';
import React, {useEffect} from 'react';

//Redux
import store from './Redux/store';
import {Provider} from 'react-redux';
// import {Provider} from 'react-redux';
// import store from './Redux/store';
import RootContainer from './RootContainer';

const App = (): JSX.Element => {
  return (
    <Provider store={store}>
      <RootContainer />
    </Provider>
  );
};

export default App;
