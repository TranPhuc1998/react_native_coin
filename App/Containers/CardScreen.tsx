import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {RootState} from '../Redux/rootReducer';
import {hScale, wScale} from '../Themes/Responsive';

type ICardScreen = {
  item: {
    id: string;
    price: number;
    image: string;
    name: string;
  };
};

const CardScreen: React.FC<ICardScreen> = ({}) => {
  //const dispatch = useDispatch();
  const cart = useSelector((state: RootState) => state.info);

  const renderItem = () => {
    return (
      <View style={styles.renderItem}>
        {cart[0].items.length > 1 && (
          <View style={{margin: 10}}>
            {cart[0].items.map(item => (
              <View>
                <Image
                  source={{uri: item.image}}
                  resizeMode="contain"
                  style={styles.image}
                />
                <Text>{item.name}</Text>
                <Text>{item.price}</Text>
              </View>
            ))}
          </View>
        )}
      </View>
    );
  };
  const renderFlatList = () => {
    return (
      <FlatList
        data={cart}
        keyExtractor={item => item.id.toString()}
        renderItem={renderItem}
      />
    );
  };

  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        flex: 1,
      }}>
      <Text>{cart[0].name}</Text>
      <View>
        {cart[0].items.length > 0 && (
          <View style={{flex: 1}}>{renderFlatList()}</View>
        )}
      </View>
    </View>
  );
};
export default CardScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#81ecec',
    alignItems: 'center',
    justifyContent: 'center',
  },
  renderItem: {
    paddingTop: hScale(30),
    paddingRight: hScale(250),
    borderRadius: 10,
    borderWidth: 0.85,
    borderColor: '#000',
  },
  image: {
    width: wScale(200),
    height: hScale(100),
  },
});
