import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  Alert,
  Dimensions,
  FlatList,
  Pressable,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Header from '../Components/Header/Header';

import {IListDish} from '../Utils/types';
import {Item} from 'react-native-paper/lib/typescript/components/List/List';
import {hScale, wScale} from '../Themes/Responsive';

import {infoActions} from '../Redux/Actions/Info/infoActions.type';
import {RootState} from '../Redux/rootReducer';
const {width} = Dimensions.get('window');
type IHomeScreen = {
  item: {
    id: string;
    price: number;
    image: string;
    name: string;
  };
};
const HomeScreen = () => {
  //Hook
  const [data, setData] = useState<IListDish>({
    id: '',
    name: '',
    items: [],
  });
  const info = useSelector((state: RootState) => state.info);
  const [totalPrice, setTotal] = useState({total: 0});
  const [totalall, setTotalall] = useState(0);
  const [changenumber, setChangeNumber] = useState(false);
  const [post, Setupdate] = useState([null]);

  // mảng phải fillter
  // object đúng cái tên là ko cần duyêt gì hết
  //dispatch

  const dispatch = useDispatch();
  const navigation = useNavigation();

  //useEffect

  useEffect(() => {
    dispatch({
      type: infoActions.GET_INFO,
      payload: {},
    });
  }, []);

  useEffect(() => {
    totalCart();
  }, [data]); // total dependancy là cái data

  const navigate = () => {
    dispatch({
      type: infoActions.ADD_TO_INFO,
      payload: {
        data: data,
        navigation: navigation,
      },
    });
  };

  const navigate2 = () => {
    navigation.navigate('CardScreen');
  };

  const totalCart = () => {
    let totalVal = 0;

    data.items.map(item => (totalVal += item.price));

    setTotalall(totalVal);
    console.log(data, 'data');
  };

  const caculatorCart = (name: string, price: number) => {
    const arr = {...totalPrice};
    if (!arr[name]) {
      arr[name] = price; // put
    } else {
      delete arr[name];
    }

    console.log(arr);
    let total = 0;
    for (let i in arr) {
      if (i !== 'total') {
        total += arr[i];
      }
    }

    arr['total'] = total;

    setTotal(arr);
  };

  // function
  const renderItem = ({item}: IHomeScreen) => {
    return (
      <TouchableOpacity
        style={styles.renderItem}
        onPress={() => {
          caculatorCart(item.name, item.price);
          setChangeNumber(true);
        }}>
        {item.image.length > 1 && (
          <View style={{margin: 10, flexDirection: 'row'}}>
            <Image
              source={{uri: item.image}}
              resizeMode="contain"
              style={styles.image}
            />
            <View style={{justifyContent: 'center'}}>
              <Text
                style={[
                  {fontSize: 20, fontWeight: 'bold'},
                  changenumber && {color: 'yellow'},
                ]}>
                {item.name}
              </Text>
              <Text
                style={{
                  color: 'yellow',
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                $ {item.price}
              </Text>
            </View>
          </View>
        )}
      </TouchableOpacity>
    );
  };
  const renderFlatList = () => {
    return (
      <FlatList
        data={data.items}
        keyExtractor={item => item.id.toString()}
        renderItem={renderItem}
      />
    );
  };

  const hello = (id: string) => {
    const ar = info.filter(item => item.id === id);
    setData(ar[0]);
  };

  const total = () => {};

  return (
    <View style={styles.container}>
      <Header textMid="Category" titleStyle={styles.textHeader} />
      <View>
        {info.length > 0 && (
          <View style={styles.textContainer}>
            {info.map(item => (
              <Pressable
                key={item.id}
                style={styles.textContainerHeader}
                onPress={() => hello(item.id)}>
                <Text>
                  {item.name}({item.items.length})
                </Text>
              </Pressable>
            ))}
          </View>
        )}
      </View>

      <View style={styles.divider}></View>
      {data?.items.length > 0 && (
        <View style={{flex: 1}}>{renderFlatList()}</View>
      )}

      <View style={styles.footer}>
        <TouchableOpacity
          onPress={() => {
            navigate();
          }}>
          <>
            <Text>
              {data.name}-{data.items.length}-{/* {totalPrice['total']} */}
              {totalall}
            </Text>
          </>
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={() => navigate2()}>
        <Text>Hello</Text>
      </TouchableOpacity>
    </View>
  );
};
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    flex: 1,
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  text: {
    color: '#000',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: wScale(20),
    width: '70%',
  },
  textContainerHeader: {},
  divider: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
    width: width - 70,
  },
  body: {
    paddingTop: hScale(20),
  },
  boderBody: {
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000',

    width: wScale(350),
    height: hScale(100),
    padding: 10,
    margin: 20,
  },
  renderItem: {
    paddingTop: hScale(30),
    paddingRight: hScale(250),
    borderRadius: 10,
    borderWidth: 0.85,
    borderColor: '#000',
  },
  image: {
    width: wScale(200),
    height: hScale(100),
  },
  footer: {},
});
