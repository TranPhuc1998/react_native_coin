import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import {setAppNavigator} from './AppNavigator';
import {AppearanceProvider} from 'react-native-appearance';
import BottomTabNavigator from './BottomTab/BottomTabNavigator';

export type RootStackParams = {
  BottomTabNavigator: undefined;
};

const Stack = createStackNavigator<RootStackParams>();

const AppNavigation = () => {
  return (
    <SafeAreaProvider>
      <AppearanceProvider>
        <NavigationContainer ref={setAppNavigator}>
          <Stack.Navigator headerMode={'none'}>
            <Stack.Screen
              name={'BottomTabNavigator'}
              component={BottomTabNavigator}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </AppearanceProvider>
    </SafeAreaProvider>
  );
};

export default AppNavigation;
