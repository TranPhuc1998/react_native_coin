import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Animated, StyleSheet} from 'react-native';

import CardScreen from '../../Containers/CardScreen';
import HomeScreen from '../../Containers/HomeScreen';

export type BottomTabParams = {
  HomeScreen: undefined;

  CardScreen: undefined;
};

const BottomTab = createBottomTabNavigator<BottomTabParams>();

const BottomTabNavigator = () => (
  <Animated.View style={StyleSheet.flatten([styles.stack])}>
    <BottomTab.Navigator tabBar={props => <BottomTabBar {...props} />}>
      <BottomTab.Screen name={'HomeScreen'} component={HomeScreen} />

      <BottomTab.Screen name={'CardScreen'} component={CardScreen} />
    </BottomTab.Navigator>
  </Animated.View>
);

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
  },
});

export default BottomTabNavigator;
