/* eslint-disable jsx-quotes */
/* eslint-disable react-hooks/exhaustive-deps */
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import ReduxNavigation from './Navigation/ReduxNavigation';

const {width, height} = Dimensions.get('screen');

const RootContainer = () => {
  return (
    <SafeAreaView style={styles.container}>
      <ReduxNavigation />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  modal: {
    position: 'absolute',
    width: width,
    height: height,
    zIndex: 1000,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000050',
  },
});

export default RootContainer;
