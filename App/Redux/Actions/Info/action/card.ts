import {NavigationAction, useNavigation} from '@react-navigation/core';
import {PayloadAction} from '@reduxjs/toolkit';
import {AxiosResponse} from 'axios';
import {call, put} from 'redux-saga/effects';
import {GET_INFO_API} from '../../../Api/getInfoApi';
import {getInfo, t, Payload, ADD_TO_INFO} from '../../../Slices/infoSlice';

export type card = Array<{
  id: string;
  name: string;
  items: Array<{
    id: string;
    price: number;
    image: string;
    name: string;
  }>;
}>;
export function* GET_INFO_CARD(_action: PayloadAction) {
  const navigation = _action.payload.navigation;
  try {
    console.log('123');
    // const arr = [..._action.payload.data];
    const test = [];
    test.push(_action.payload.data);
    console.log(_action.payload.data, 'arr');
    console.log(test, 'test');
    yield put(ADD_TO_INFO({data: test}));
    navigation.navigate('CardScreen');
    // console.log(arr, 'arr');
  } catch (error) {
    console.log(error);
  }
}
