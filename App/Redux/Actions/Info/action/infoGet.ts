import {PayloadAction} from '@reduxjs/toolkit';
import {AxiosResponse} from 'axios';
import {call, put} from 'redux-saga/effects';
import {GET_INFO_API} from '../../../Api/getInfoApi';
import {getInfo, t, Payload} from '../../../Slices/infoSlice';

// export type t1 = Array<{
//   id: string;
//   name: string;
//   items: Array<{
//     id: string;
//     price: number;
//     image: string;
//     name: string;
//   }>;
// }>;
export function* GET_INFO(_action: PayloadAction) {
  try {
    const response: AxiosResponse<any> = yield call(() => GET_INFO_API());
    if (response) {
      // xủe lý logic
      yield put(getInfo({data: response.data}));
      // console.log('hello', response);
    }
  } catch (error) {}
}
