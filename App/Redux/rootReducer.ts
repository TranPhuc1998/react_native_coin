import {combineReducers} from '@reduxjs/toolkit';
import infoReducer from './Slices/infoSlice';
const rootReducer = combineReducers({
  info: infoReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
