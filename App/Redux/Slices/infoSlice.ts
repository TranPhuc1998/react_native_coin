import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {act} from 'react-test-renderer';

export type Info = Array<{
  id: string;
  name: string;
  items: Array<{
    id: string;
    price: number;
    image: string;
    name: string;
  }>;
}>;

const initialState = [] as Info;

export type t = {
  data: Array<{
    id: string;
    name: string;
    items: Array<{
      id: string;
      price: number;
      image: string;
      name: string;
    }>;
  }>;
};
const info = createSlice({
  name: 'info',
  initialState,
  reducers: {
    getInfo: (state, action: PayloadAction<t>) => {
      try {
        state = [...action.payload.data];
        return state;
      } catch (error) {
        console.log(error);
      }
    },
    ADD_TO_INFO: (state, action: PayloadAction<t>) => {
      state = action.payload.data;
      // console.log('state', state);
      return state;
    },
  },
});

const {reducer, actions} = info;
export const {getInfo, ADD_TO_INFO} = actions;
export default reducer;
