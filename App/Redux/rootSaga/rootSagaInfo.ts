import {takeEvery} from 'redux-saga/effects';
import {GET_INFO_CARD} from '../Actions/Info/action/card';
import {GET_INFO} from '../Actions/Info/action/infoGet';
import {infoActions} from '../Actions/Info/infoActions.type';

export default function* rootSagaUser() {
  yield takeEvery(infoActions.GET_INFO, GET_INFO);
  yield takeEvery(infoActions.ADD_TO_INFO, GET_INFO_CARD);
}
