import {BASE_URL} from '../../Service/endpoint';
import {GET_AXIOS} from '../../Service/apiCaller';

export const GET_INFO_API = async () => {
  return await GET_AXIOS(BASE_URL, {});
};
