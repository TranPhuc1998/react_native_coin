const fonts = {
    bold: 'Quicksand-Bold',
    light: 'Quicksand-Light',
    medium: 'Quicksand-Medium',
    regular: 'Quicksand-Regular',
    semiBold: 'Quicksand-SemiBold',
};

export default fonts;
