import DefaultTheme from './DefaultTheme';
import DarkTheme from './DarkTheme';
import Colors from './Colors';
// import Images from './Images'
import Fonts from './Fonts';

export {
    DefaultTheme,
    DarkTheme,
    Colors,
    // Images,
    Fonts,
};
